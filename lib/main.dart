import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String myanmarGreeting = "ဟဲလို လေလွင့်";
String koreaGreeting = "헬로 플러터";
class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;

    @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter!!"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting?
                  spanishGreeting:englishGreeting;
                });
              }, icon: Icon(Icons.scatter_plot_rounded)),
              IconButton(onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting?
                  myanmarGreeting:englishGreeting;
                });
              }, icon: Icon(Icons.sentiment_satisfied_alt_sharp)),
              IconButton(onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting?
                  koreaGreeting:englishGreeting;
                });
              }, icon: Icon(Icons.sentiment_very_satisfied_sharp )),
            ],
          ),
          body: Center(
            child: Text(displayText,
            style: TextStyle(fontSize: 40, color: Colors.red, fontWeight: FontWeight.bold),),
          ),
        ),
    );
  }
}



// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           appBar: AppBar(
//             title: Text("Hello Flutter"),
//             leading: Icon(Icons.home),
//             actions: <Widget>[
//               IconButton(onPressed: (){}, icon: Icon(Icons.refresh))
//             ],
//           ),
//           body: Center(
//             child: Text("Hello Flutter!",
//             style: TextStyle(fontSize: 40, color: Colors.red, fontWeight: FontWeight.bold),),
//           ),
//         ),
//     );
//   }
// }
    // home: SafeArea(
    //   child: Text("Hello Flutter"),
    // )
    // return MaterialApp(
    //   home: Center(
    //     child: Text("Hello Flutter"),
    //   ),
    // );
